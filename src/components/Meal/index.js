import React from 'react';

import './style.css';

const Meal = ({ names, categorys, areas, thumbnails, instructions }) => {

    return (
        <ul className="Card">
            <li className="CardItem">
                <div className="CardMeal">
                    <div className="MealImage"><img src={thumbnails}></img></div>
                    <div className="CardContent">
                        <div className="MealContainer">
                            <h2 className="MealName">{names}</h2>
                            <p className="MealInfo">Area: <span className="MealSpan">{areas}</span></p>
                            <p className="MealInfo">Category: <span className="MealSpan">{categorys}</span></p>
                        </div>
                        <div className="InstructionContainer">
                            <p className="MealInstructions">Instructions:</p>
                            <p className="MealInstructionsText">{instructions}</p>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    )
}

export default Meal;