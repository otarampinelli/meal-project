import React from 'react';

import Meal from '../Meal';
import './style.css';

const Meals = (props) => {
    const { strMeal, strCategory, strArea, strMealThumb, strInstructions } = props;

    return (
        <div>
            <div className="Main">
                <Meal
                    names={strMeal}
                    categorys={strCategory}
                    areas={strArea}
                    thumbnails={strMealThumb}
                    instructions={strInstructions}
                />
            </div>
            )
        </div>
    )
}

export default Meals;