import React from 'react';

import './style.css';

const Search = ({ type, placeholder, setSearch }) => {

    return (
        <div>
            <div className="Search">
                <input
                    className="SearchInput"
                    type={type}
                    placeholder={placeholder}
                    onChange={(e) => setSearch(e.target.value)}
                />
            </div>
        </div>
    )
}

export default Search;