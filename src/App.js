import { React, useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import Axios from 'axios';

import Meals from './components/Meals';
import Search from './components/Search';
import './style.css';

const App = () => {
    const [items, setItems] = useState([]);
    const [search, setSearch] = useState("");
    const [filteredMeals, setFilteredMeals] = useState([]);

    useEffect(() => {
        Axios.get('https://www.themealdb.com/api/json/v1/1/search.php?s=Chicken').then(response => {
            setItems(response.data.meals);
        })
    }, [])

    useEffect(() => {
        setFilteredMeals(
            items.filter((item) =>
                item.strMeal.toLowerCase().includes(search.toLowerCase())
            )
        );
    }, [search, items]);

    return (
        <div>
            <Search
                type="text"
                placeholder="Search Meals"
                setSearch={setSearch}
            />
            {filteredMeals.map((item) => (
                <div>
                    <Meals {...item} />
                </div>
            ))}
        </div>
    )
}

export default App;